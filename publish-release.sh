#!/bin/bash

set -euo pipefail

img="$1"
dir="$2"

version="$(basename "$dir")"

mkdir "$dir"
xz -0vT0 <"$img" >"$dir/v-i.img.xz"
cp configure-installer write-and-config.sh "$dir/"
for file in NEWS.md README.md tutorial.md spec.md; do
	sed "s/VERSION/$version/" "$file" >tmp.md
	pandoc tmp.md -o "$dir/$(basename "$file" .md).html"
	rm tmp.md
done
