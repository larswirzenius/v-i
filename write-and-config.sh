#!/bin/bash

set -euo pipefail

config="$1"
dev="$2"
img="$3"

if ! blkid "$dev" >/dev/null; then
	echo "$dev is not a block device" 1>&2
	exit 1
fi

echo "Unmounting everything in $dev"
umount -fv "$dev"* || true
echo "Write $img to $dev"
dd if="$img" bs=1M oflag=direct status=progress of="$dev"
sync

for fs in "$dev"?; do
	echo
	echo "Fsck file system on $fs"
	fsck -f -C0 "$fs"
done

echo
echo "Configure installer"
python3 ./configure-installer "$config" "$dev"
