# v-i spec file spec

The input file to `v-i`, called the "spec file", is a specification of
the installation target. This is the specification of that file.

Overview:

* it's a YAML file in the form of a key/value map
* it specifies things the installer needs to know
* unknown keys are errors

The keys that the installer understands are:

* `hostname`---the name of the installed system, what `hostname` will output
* `drive`---the boot drive
* `extra_drives`---additional drives to format and add to the volume group
* `extra_lvs`---additional logical volumes to create
* `extra_playbooks`---additional Ansible playbooks to run
* `ansible_vars`---variables for Ansible
* `ansible_vars_files`---files with variables for Ansible
* `luks`---cleartext password for full disk encryption
* `debian_release`---which release of Debian to install
* `root_fstype`---file system type of the root file system

The `hostname` and `drive` fields are required, everything else is
optional. No field may exist more than once.

The fields are described in more detail in sections below.

* `hostname`

    The name of the installed system. This gets put into `/etc/hostname`
    which the system will read when it boots.

    A plain text string. The installer doesn't verify it, but if the
    contents isn't suitable, the target system name will be wrong.

    **Example:**
    
    ~~~yaml
    hostname: exolobe1
    ~~~

* `drive`

    The full path name of the block device for the boot drive of the
    installed system, as seen by the installer. The boot loader will
    be installed on this drive, `/boot` will be a partition, and the
    rest of the drive will be a physical volume for the volume group
    the installer creates. The drive will effectively be wiped:
    partitions removed, physical volumes removed, all blocks on SSDs
    discarded, etc, but not overwritten.

    **Example:** first SATA drive
    
    ~~~yaml
    drive: /dev/sda
    ~~~

    **Example:** first NVMe drive
    
    ~~~yaml
    drive: /dev/nvme0n1
    ~~~

* `extra_drives`

    A list of full path names of block devices of additional drives to
    use as physical volumes in the volume group the installer creates.
    The drives will be wiped the same way as the boot drive.
    
    The installer **will remove** all volume groups and physical
    volumes even on drives not mentioned in `drive` or `extra_drives`.
    It will discard all blocks only in the named drives, however.

    **Example:**
    
    ~~~yaml
    extra_drives:
    - /dev/sdb
    - /dev/sd
    ~~~

* `extra_lvs`

    Additional logical volumes for the installer to create. The value
    for this key is a list of key/value mappings describing the Luvs to
    create, where the inner keys are:
    
    * `name`---name of of the LV
    * `size`---size of the LV, in a format accepted by `lvcreate --size`
    * `fstype`---type of the file system for the LV, defaults to `ext4`
    * `mounted`---where the LV should be mounted

    **Example:**
    
    ~~~yaml
    extra_lvs:
      - name: home
        size: 202G
        fstype: btrfs
        mounted: /home
    ~~~

* `extra_playbooks`

    A list of additional Ansible playbooks to run on the installed system. The
    installer first runs its own standard playbook, and the additional
    playbooks can make changes after that. The playbooks need to be
    present in the installer.

    **Example:**
    
    ~~~yaml
    extra_playbooks:
    - foo.yml
    ~~~

* `ansible_vars`

    Variable definitions for Ansible playbooks. Note that these will
    be set  for all playbooks the installer runs.

    **Example:** `exolobe1`
    
    ~~~yaml
    ansible_vars:
      user_pub: |
       ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPQe6lsTapAxiwhhEeE/ixuK+5N8esCsMWoekQqjtxjP
    ~~~

* `ansible_vars_files`

    Like `ansible_vars`, but list of files with variable definitions
    to give to Ansible.

    **Example:**
    
    ~~~yaml
    ansible_vars_files:
      - hostid.yml
    ~~~

* `luks`

    The cleartext password for full disk encryption (LUKS). You
    probably want to change this after installing, but for very high
    risk situations, be aware the reading SSD flash chips can let an
    attacker access the old LUKS header after the change.

    **Example:**
    
    ~~~yaml
    luks: asdf
    ~~~

* `debian_release`

    Code name of Debian release to install. Defaults to `bullseye`.

    **Example:**
    
    ~~~yaml
    debian_release: bookworm
    ~~~

* `root_fstype`

    Type of the root file system. Defaults to `ext4`. (This is a
    little-tested feature of `v-i`. Please report if it works for
    you.)

    **Example:**
    
    ~~~yaml
    root_fstype: xfs
    ~~~
