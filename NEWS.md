# Release notes for v-i

This file summarizes user-visible changes between releases of v-i, the
vmdb2-based installer of Debian onto bare metal systems.


# Version 0.4, released 2023-08-13

Brown paper bag release.

* Fix tutorial to actually work. Some examples were wrong or obsolete.
  Some details were missing. Also, simplify things a little by
  reducing unnecessary friction: for example, the download links now
  work to the release version.

* Fix `configure-installer` to deal with no host key or user CA key
  being set.

* Fix published files to be HTML, not Markdown, so they are easier to
  read.


# Version 0.3, released 2023-08-12

## Major changes

* Both the installer can now use wifi. The wifi password used in the
  installer is copied to the target system. It's now possible to
  install without wired networking at all, and the booted system
  connects to wifi automatically.

* SSH host key and host certificate can be set at installation time.
  This helps connecting to newly installed systems, for show using
  certificates. Also, users can log in using SSH user certificates
  instead of keys, if they have certificates made with trusted SSH CA
  keys.

* The installer provides addresses via its on Ethernet via DHCP, to
  allow bootstrapping a LAN. It avoids serving addresses via its main
  Ethernet port, which it uses itself to access the Internet.

* File systems other then ext4 are now supported on the target
  system, for example btrfs.

* The installer attempts to use "secure discard", when emptying
  drives. It falls back to plain discard, without an error, if the
  secure variant fails.

## Minor changes

* Root can now only log in via SSH using a key or certificate:
  passwords are not allowed for root on SSH. Console login as root
  doesn't require a password.

* Ansible variables can be given via files, not just in playbooks,
  when running the installer.

* The installer now allows installing any Debian release. Debian 11
  and 12 have been tested.

* Improved documentation of the target specification file format and
  how to use SSH certificates.

* Various sharp corners have been rounded to make development and use
  of the installer less painful.


# Version 0.2, released 2022-08-07

## New or changed features

* New script `configure-installer` allows configuring an installer
  image that has been written to a USB drive or other block device. It
  replaced the older `set-authorized-key` script. It adds support for
  SSH CA and host host certificates for the installer, and accepting
  user certificates for logging into the installer.

* The tutorial is a little improved and will hopefully be possible to
  understand even if you don't already know everything.

* The installer image now boots a little faster: it now uses
  `systemd-networkd` instead of `iupdown` to bring up the network, and
  the persistent `journald` system log is disabled.

* The `/etc/v-i-version` file contains information about the version
  of the `v-i` repository used to build the installer image.

## Bug fixes

* `ping` now works on the installer.

## Other notable changes

* The `rootfs.tar.gz` tarball that used to be included in the
  installer image is gone. It wasn't useful for anything but building
  an installer image.

# Version 0.1, released 2022-03-06

This is the first release of v-i. It may or may not work for you. It
will reliably destroy all your data.
