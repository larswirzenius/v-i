# Using SSH host and user certificates with v-i

`v-i` supports the use of SSH certificates so that you never need to
accept a new host key manually, or install your public key into
`authorized_keys`. By using certificates, you can just log into the
installer with SSH, and to the installed system, without answering
questions or using passwords.

[SSH CA](https://liw.fi/sshca/) is a way to signed public keys for
host and user authentication. They remove the need for users to verify
an SSH host key, or to manage `authorized_keys` files. The SSH server
and client are configured to trust certificates made using a CA key,
so that the client trusts a server key if the certificate checks out,
and the server allows a client to log in likewise. This is convenient
for situations where a host key changes, or a host is newly installed.
This is the case with `v-i`, and thus `v-i` enables, but doesn't
require, the use of SSH certificates.

When installing a brand new system, where the installer isn't trusted
to have a CA private key, we create a temporary host key and a
corresponding short-lived host certificate for the target system. This
will allow installation of the system and also configuring it to have
a strong, secure host key and longer-lived certificate. The window of
opportunity for an attacker to misuse the temporary host key is
limited by the lifetime of the certificate created for it. After the
new host key is installed, the temporary one won't be used. Thanks to
certificates, nobody cares that the host key changed, because the new
key is also certified.


## Create a CA key pair

You can use your existing CA key, if you have one. We document this
step for completeness.

You may want to have separate CA key pairs for users and hosts, or one
just for use with `v-i`, but for simplicity, were only using one in
this document.

```sh
ssh-keygen -t ed25519 -C "my CA" -f ~/.ssh/ca.ssh
```

You can change the final filename on the above command to put the CA
key wherever you want.


## Create a user certificate

You'll need this to log into the installer, and the installed system.

~~~sh
ssh-keygen -s ca.ssh -I "my CA" -n "$USER" ~/.ssh/id_ed25519.pub
~~~

Replace the final filename in the above command with the path to your
actual SSH public key. The certificate will be put next to the public
key with a `-cert.pub` suffix.


## Set up certificates for the v-i installer itself

To set an SSH host certificate for the v-i installer image:

* generate a host key for the installer
  - `ssh-keygen -t ed25519 -N "" -C "host v-i" -f installer.ssh`
* using your SSH CA key, create a host certificate
  - `ssh-keygen -s ~/.ssh/ca.ssh -I "my CA" -h -n v-i installer.ssh.pub`
* copy your CA public key to `ca.ssh.pub`
  - `cp ~/.ssh/ca.pub .`
* in the config file for the `configure-installer` script, add the
  following lines:
  - `user_ca_pub_file: ca.ssh.pub`
  - `host_key_file: installer.ssh`
  - `host_cert_file: installer.ssh-cert.pub`
* then configure the installer as usual


## Set up certificates for the installed system

We use the name `x220` for the target system to be installed. Replace
what with your actual host's name.

* Generate a temporary host key for the installed system.
  - `ssh-keygen -t ed25519 -N "" -C "host x220" -f x220.ssh`
* Create a short-lived host certificate. Ten minutes should be enough
  to get the system installed and a new host key and certificate
  installed. Adjust the `-V` argument as needed.
  - `ssh-keygen -s ~/.ssh/ca.ssh -I "my CA" -h -n x220 -V +10m x220.ssh.pub`
* In your `v-i` spec file for your target, add the following to the
  `ansible_vars` section:

~~~yaml
ansible_vars:
  user_ca_pubkey: |
    sk-ssh-ed25519@openssh.com whatever
  host_key: |
    -----BEGIN OPENSSH PRIVATE KEY-----
    whatever
    -----END OPENSSH PRIVATE KEY-----
  host_cert: |
    ssh-ed25519-cert-v01@openssh.com whatever
~~~

Note that for the spec file, the values need to be copied into the
file itself.

Then do the install, boot into the installed system, and install a new
host key and certificate using your preferred configuration management
system.
