#!/bin/bash

set -eu -o pipefail

tarball="$1"

vmdb2 --output installer.img --log installer.log installer.vmdb \
	--verbose --rootfs-tarball "$tarball"
chmod a+r installer.img
