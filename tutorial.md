# Install Debian on a bare metal system with v-i

You need:

- a USB drive, at least 4 GB
- a PC that supports UEFI
- backups: _all drives_ in the PC will be wiped from data

The steps:

1. Download the installer image (`v-i.img.xz`) and related scripts
   (`configure-installer` and `write-and-config.sh`) from a
   subdirectory or <https://files.liw.fi/v-i/>.
   
   * <https://files.liw.fi/v-i/VERSION/v-i.img.xz>
   * <https://files.liw.fi/v-i/VERSION/configure-installer>
   * <https://files.liw.fi/v-i/VERSION/write-and-config.sh>

2. Unpack the downloaded image.

   ~~~sh
   unxz v-i.img.xz
   ~~~
   
   This will use a few gigabytes of disk apace.

3. Write a configuration file for the installer, to allow logging in
   via SSH to the running installer. If you don't intend to do that,
   you can just create an empty file. Otherwise, create a YAML file
   like the one below, but with values customized for you. You need
   one of `authorized_keys_file` or `user_ca_pub_file`.

   ~~~yaml
   user_ca_pub_file: ~liw/.ssh/ca/userca.pub
   authorized_keys_file: ~liw/.ssh/id_personal.pub
   ~~~
   
   Either of these two lines configures the installer SSH to allow you
   to log in as `root` using SSH. Password logins are not allowed for
   SSH. You can also log in via the console, as `root`, without a
   password.
   
   The installer configuration file can specify a wifi access point to
   connect to, if you need that.
   
   ~~~yaml
   wifi_name: DadItIsThisOne
   wifi_password: hunter2
   ~~~

4. Insert USB drive and find out the device allocated it. I prefer GNOME
   Disks for this, but you can also look up the device of the USB
   stick: run `sudo dmsg -T`. The device name is usually `/dev/sdx`
   for some value of x.

5. Write the installer image to the USB drive, and configure it.

   ~~~sh
   sudo bash write-and-config.sh config.yaml /dev/sdx v-i.img
   ~~~

   This will take a few minutes.

6. Make sure everything has been written to the USB drive. Again, I
   prefer GNOME Disks for this, but from the command line:

    ~~~sh
    udisksctl power-off -b /dev/sdx
    ~~~

7. Move the USB drive to the target machine and boot off the drive.
   How this happens varies from machine to machine. On my Thinkpad
   X220, I press F12 at the POST screen and then choose the right
   drive from the boot menu.

8. Wait for the system to boot. Log in as root, either from the
   virtual console (no password), or via SSH.

   If you need to connect to wifi, you need to log in as root and run
   the following commands:

    ~~~sh
    iwctl station wlan0 get-networks
    iwctl station wlan0 connect DadItIsThisOne
    ~~~

   Substitute the name of the network as needed. Enter wifi password
   when requested.

9. Create a target specification file `foo-target.yaml`. The name can
   be anything, but it must be valid YAML. In the example below, there
   is one NVMe drive that will get the system installed. All drives
   will be encrypted using LUKS, with the password "hunter2".

    ~~~yaml
    hostname: x220
    drive: /dev/nvme0n1
    luks: hunter2
    ansible_vars:
      user_pub: |
       ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPQe6lsTapAxiwhhEeE/ixuK+5N8esCsMWoekQqjtxjP
    ~~~
    
    The SSH public key will be added to the root user's
    `authorized_keys` file. See [spec.md][] for a full description of
    the target specification file.

10. Install. Add the `--verbose` option to `./v-i` if you want to know
    what's happening. The first time you run this on a given v-i USB
    drive, it takes a while: it runs `vmdebootstrap` and that is just
    slow. However, the output is cached, so further runs are faster.

    ~~~sh
    rm -f install.log
    v-i --verbose foo-target.yaml
    ~~~
     
    This will take few minutes the first time, assuming a fast
    Internet connection. Further runs go faster.

11. Reboot into the installed system.

12. Finish configuring the system in whatever way you like to do that.
    I use Ansible.

    Remember to change the LUKS password for each drive.

13. Optional: Let me know how it went.

[spec.md]: spec.html

